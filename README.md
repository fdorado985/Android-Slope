<p align="center">
  <img src=".assets/logo.png" title="Logo">
</p>

---

[![platform_android](.assets/platform.png)](https://developer.android.com/about/)
[![ide_android_studio](.assets/ide.png)](https://developer.android.com/studio/)
[![twitter](.assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](.assets/instagram.png)](https://www.instagram.com/juan_fdorado)

On this repo we are going to save some little projects, that we help us to learn the most basics steps of Android Development, there is gonna be a README file to explain some little stuffs on each project.

I invite you to take a look at them

Here are the projects and a little description of what are they made it for. Enjoy it and most important KEEP CODING!

## Android N: From Beginner To Paid Professional
* [Java-Basics](Projects/java/java_basics) - Learn the basics of Java for Android Development.
* [CoolCalc](Projects/CoolCalc) - The basics of Android.
* [FitOrFlab](Projects/FitOrFlab) - Working with `Activities`.
* [HeroMe](Projects/HeroMe) - `Fragments` & Mastering Android `Layouts`.
* [Devslopes-Radio](Projects/DevslopesRadio) - `Grids` & `List` with `Recycler View`.
* [Bootcamp-Locator](Projects/BootcampLocator) - `Maps` & `GPS`.
* [Funshine](Projects/Funshine) - `Web Requests` & `REST`.
* [FB Chat](Projects/FBChat) - Building a `Login` App With `Firebase`.
* [Mapbox](Projects/MapboxJavaDemo) - `Mapbox` : How to use the third party maps!

## Kotlin For Android
* [Kotlin-Basics](Projects/KotlinFoundation) - Learn the basics of `Kotlin` for Development.
* [DinnerDecider](Projects/DinnerDecider) - Creating your first Android App.
* [DevProfile](Projects/DevProfile) - Learn about how is the behaviour of `Layouts`.
* [Swoosh](Projects/Swoosh) - `Activity Lifecycle`... here you'll see some `Toast` messages, `Extras`, `Intents` and more.
* [CoderSwag](Projects/CoderSwag) - `List` and `Recycler Views`.
* [Smack](Projects/Smack) - Slack Clone : Smack Chat App.
* [Mapbox](Projects/mapboxnavigationdemo) - `Mapbox` : How to use the third party maps!.
