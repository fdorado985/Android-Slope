package com.dorado.smack.Controller

import android.app.Application
import com.dorado.smack.Utilities.SharedPrefs

class App : Application() {

  // This works as a kind of singleton

  companion object {
    lateinit var prefs: SharedPrefs
  }

  // View cycle

  override fun onCreate() {
    prefs = SharedPrefs(applicationContext)
    super.onCreate()
  }
}