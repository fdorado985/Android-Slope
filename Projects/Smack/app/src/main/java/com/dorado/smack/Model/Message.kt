package com.dorado.smack.Model

class Message(val message: String, val username: String, val channelId: String, val userAvatar: String, val userAvatarColor: String, val id: String, val timeStamp: String)