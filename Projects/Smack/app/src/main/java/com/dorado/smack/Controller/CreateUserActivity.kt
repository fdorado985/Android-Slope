package com.dorado.smack.Controller

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import android.widget.Toast
import com.dorado.smack.R
import com.dorado.smack.Services.AuthService
import com.dorado.smack.Utilities.BROADCAST_USER_DATA_CHANGED
import kotlinx.android.synthetic.main.activity_create_user.*
import java.util.*

class CreateUserActivity : AppCompatActivity() {

  // Properties

  var userAvatar = "profileDefault"
  var avatarColor = "[0.5, 0.5, 0.5, 1]"

  // View cycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_create_user)
    createSpinner.visibility = View.INVISIBLE
  }

  // onClick Functions

  fun generateUserAvatar(view: View) {
    val random = Random()
    val color = random.nextInt(2)
    val avatar = random.nextInt(28)

    if (color == 0) {
      userAvatar = "light$avatar"
    } else {
      userAvatar = "dark$avatar"
    }

    val resourceId = resources.getIdentifier(userAvatar, "drawable", packageName)
    createAvatarImageView.setImageResource(resourceId)
  }

  fun generateColorClicked(view: View) {
    val random = Random()
    val r = random.nextInt(255)
    val g = random.nextInt(255)
    val b = random.nextInt(255)

    createAvatarImageView.setBackgroundColor(Color.rgb(r, g, b))

    val savedR = r.toDouble() / 255
    val savedG = g.toDouble() / 255
    val savedB = b.toDouble() / 255

    avatarColor = "[$savedR, $savedG, $savedB, 1]"
  }

  fun createUserClicked(view: View) {
    enableSpinner(true)
    val username = createUsernameText.text.toString()
    val email = createEmailText.text.toString()
    val password = createPasswordText.text.toString()

    if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()) {
      AuthService.registerUser(email, password) { registerSuccess ->
        if (registerSuccess) {
          AuthService.loginUser(email, password) { loginSuccess ->
            if (loginSuccess) {
              AuthService.createUser(username, email, userAvatar, avatarColor) { createSuccess ->
                if (createSuccess) {
                  val userDataChange = Intent(BROADCAST_USER_DATA_CHANGED)
                  LocalBroadcastManager.getInstance(this).sendBroadcast(userDataChange)
                  enableSpinner(false)
                  finish()
                } else {
                  errorToast()
                }
              }
            } else {
              errorToast()
            }
          }
        } else {
          errorToast()
        }
      }
    } else {
      Toast.makeText(this, "Make sure username, email and password are filled up", Toast.LENGTH_SHORT).show()
      enableSpinner(false)
    }
  }

  fun errorToast() {
    Toast.makeText(this, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show()
    enableSpinner(false)
  }

  fun enableSpinner(enable: Boolean) {
    if (enable) {
      createSpinner.visibility = View.VISIBLE
    } else {
      createSpinner.visibility = View.INVISIBLE
    }

    createUserBtn.isEnabled = !enable
    createAvatarImageView.isEnabled = !enable
    backgroundColorBtn.isEnabled = !enable
  }
}
