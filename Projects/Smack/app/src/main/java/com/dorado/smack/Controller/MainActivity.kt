package com.dorado.smack.Controller

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.service.autofill.UserData
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.EditText
import com.dorado.smack.Adapters.MessageAdapter
import com.dorado.smack.Model.Channel
import com.dorado.smack.Model.Message
import com.dorado.smack.R
import com.dorado.smack.Services.AuthService
import com.dorado.smack.Services.MessageService
import com.dorado.smack.Services.UserDataService
import com.dorado.smack.Utilities.BROADCAST_USER_DATA_CHANGED
import com.dorado.smack.Utilities.SOCKET_URL
import io.socket.client.IO
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.*

class MainActivity : AppCompatActivity() {

  // Properties

  val socket = IO.socket(SOCKET_URL)
  lateinit var channelAdapter: ArrayAdapter<Channel>
  lateinit var messageAdapter: MessageAdapter
  var selectedChannel: Channel? = null

  private val userDataChangeReceiver = object : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
      if (App.prefs.isLoggedIn) {
        val name = UserDataService.name
        val email = UserDataService.email
        val avatarName = UserDataService.avatarName
        val avatarColor = UserDataService.avatarColor

        userNameNavHeader.text = name
        println("userNameNavHeader.text = $name")
        userEmailNavHeader.text = email
        println("userEmailNavHeader.text = $email")
        val resourceId = resources.getIdentifier(avatarName, "drawable", packageName)
        userImageNavHeader.setImageResource(resourceId)
        userImageNavHeader.setBackgroundColor(UserDataService.returnAvatarColor(avatarColor))
        loginBtnNavHeader.text = "Logout"

        MessageService.getChannels { complete ->
          if (complete) {
            if (MessageService.channels.count() > 0) {
              selectedChannel = MessageService.channels[0]
              channelAdapter.notifyDataSetChanged()
              updateWithChannel()
            }
          }
        }
      }
    }
  }

  private val onNewChannel = Emitter.Listener { args ->
    if (App.prefs.isLoggedIn) {
      runOnUiThread {
        val channelName = args[0] as String
        val channelDescription = args[1] as String
        val channelId = args[2] as String

        val newChannel = Channel(channelName, channelDescription, channelId)
        MessageService.channels.add(newChannel)
        channelAdapter.notifyDataSetChanged()
      }
    }
  }

  private val onNewMessage = Emitter.Listener { args ->
    if (App.prefs.isLoggedIn) {
      runOnUiThread {
        val msgBody = args[0] as String
        val channelId = args[2] as String
        val username = args[3] as String
        val userAvatar = args[4] as String
        val userAvatarColor = args[5] as String
        val id = args[6] as String
        val timeStamp = args[7] as String

        if (channelId == selectedChannel?.id) {
          val newMessage = Message(msgBody, username, channelId, userAvatar, userAvatarColor, id, timeStamp)
          MessageService.messages.add(newMessage)
          messageAdapter.notifyDataSetChanged()
          messageListView.smoothScrollToPosition(messageAdapter.itemCount - 1)
        }
      }
    }
  }

  // View cycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setSupportActionBar(toolbar)

    // Configure the view to initialize UI
    setupView()

    // Socket
    socket.connect()
    socket.on("channelCreated", onNewChannel)
    socket.on("messageCreated", onNewMessage)

    // Register Broadcast Activity
    LocalBroadcastManager.getInstance(this).registerReceiver(userDataChangeReceiver, IntentFilter(BROADCAST_USER_DATA_CHANGED))

    val toggle = ActionBarDrawerToggle(
        this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
    drawer_layout.addDrawerListener(toggle)
    toggle.syncState()

    // Setup the Adapter for the channel list
    setupAdapters()

    // Listener to List Item
    channel_list.setOnItemClickListener { _, _, i, _ ->
      selectedChannel = MessageService.channels[i]
      drawer_layout.closeDrawer(GravityCompat.START)
      updateWithChannel()
    }

    // Use shared prefs to relogin
    if (App.prefs.isLoggedIn) {
      AuthService.findUserByEmail(this) {}
    }

  }

  override fun onDestroy() {
    socket.disconnect()
    LocalBroadcastManager.getInstance(this).unregisterReceiver(userDataChangeReceiver)
    super.onDestroy()
  }

  // OnClick Functions

  override fun onBackPressed() {
    if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
      drawer_layout.closeDrawer(GravityCompat.START)
    } else {
      super.onBackPressed()
    }
  }

  fun loginBtnNavClicked(view: View) {
    if (App.prefs.isLoggedIn) {
      // Log out
      channelAdapter.notifyDataSetChanged()
      messageAdapter.notifyDataSetChanged()
      UserDataService.logout()
      userNameNavHeader.text = "Login"
      userEmailNavHeader.text = ""
      userImageNavHeader.setImageResource(R.drawable.profiledefault)
      userImageNavHeader.setBackgroundColor(Color.TRANSPARENT)
      loginBtnNavHeader.text = "Login"
      mainChannelName.text = "Please log in"
    } else {
      val loginIntent = Intent(this, LoginActivity::class.java)
      startActivity(loginIntent)
    }
  }

  fun addChannelClicked(view: View) {
    if (App.prefs.isLoggedIn) {
      val builder = AlertDialog.Builder(this)
      val dialogView = layoutInflater.inflate(R.layout.add_channel_dialog, null)

      builder.setView(dialogView)
          .setPositiveButton("Add") { _, _ ->
            val nameTextField = dialogView.findViewById<EditText>(R.id.addChannelNameTxt)
            val descTextField = dialogView.findViewById<EditText>(R.id.addChannelDescTxt)
            val channelName = nameTextField.text.toString()
            val channelDesc = descTextField.text.toString()

            // Create channel with the channel name and description
            socket.emit("newChannel", channelName, channelDesc)
          }
          .setNegativeButton("Cancel") { _, _ ->
            // Cancel and hide dialog

          }.show()
    }
  }

  fun sendMessageBtnClicked(view: View) {
    if (App.prefs.isLoggedIn && messageTextField.text.isNotEmpty() && selectedChannel != null) {
      val userId = UserDataService.id
      val channelId = selectedChannel?.id
      socket.emit("newMessage", messageTextField.text.toString(), userId, channelId, UserDataService.name, UserDataService.avatarName, UserDataService.avatarColor)
      messageTextField.text.clear()
      hideKeyboard()
    }
  }

  // Functions

  private fun setupView() {
    // FIXME: Without this initialization the Broadcast doesn't work!
    userNameNavHeader.text = "Login"
    userEmailNavHeader.text = ""
    userImageNavHeader.setImageResource(R.drawable.profiledefault)
    userImageNavHeader.setBackgroundColor(Color.TRANSPARENT)
    loginBtnNavHeader.text = "Login"
  }

  private fun setupAdapters() {
    channelAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, MessageService.channels)
    channel_list.adapter = channelAdapter

    messageAdapter = MessageAdapter(this, MessageService.messages)
    messageListView.adapter = messageAdapter
    val layoutManager = LinearLayoutManager(this)
    messageListView.layoutManager = layoutManager
  }

  private fun hideKeyboard() {
    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    if (inputManager.isAcceptingText) {
      inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
    }
  }

  private fun updateWithChannel() {
    mainChannelName.text = "#${selectedChannel?.name}"
    if (selectedChannel != null) {
      MessageService.getMessages(selectedChannel!!.id) { complete ->
        if (complete) {
          messageAdapter.notifyDataSetChanged()
          if (messageAdapter.itemCount > 0) {
            messageListView.smoothScrollToPosition(messageAdapter.itemCount - 1)
          }
        }
      }
    }
  }
}
