# Smack
This is a little clone of the popular app **Slack** you are able to...
* Create Users
* Login with an existing account
* Create Channels
* Send Messages
* All in real time

What do we use special here?
* Socket IO (Android Client)

## Demo
![smack_demo](.screenshots/smack_demo.gif)
