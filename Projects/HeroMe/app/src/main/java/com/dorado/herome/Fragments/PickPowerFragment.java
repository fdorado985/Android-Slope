package com.dorado.herome.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dorado.herome.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PickPowerFragment.PickPowerInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PickPowerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickPowerFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // Buttons
    private Button btnTurtlePower;
    private Button btnLightningPower;
    private Button btnFlightPower;
    private Button btnWebSlingingPower;
    private Button btnLaserVisionPower;
    private Button btnSuperStrengthPower;
    private Button btnShowBackstory;

    private PickPowerInteractionListener mListener;

    public PickPowerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PickPowerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PickPowerFragment newInstance(String param1, String param2) {
        PickPowerFragment fragment = new PickPowerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pick_power, container, false);

        btnTurtlePower = (Button)view.findViewById(R.id.btnTurtlePower);
        btnLightningPower = (Button)view.findViewById(R.id.btnLightningPower);
        btnFlightPower = (Button)view.findViewById(R.id.btnFlightPower);
        btnWebSlingingPower = (Button)view.findViewById(R.id.btnWebSlingingPower);
        btnLaserVisionPower = (Button)view.findViewById(R.id.btnLaserVisionPower);
        btnSuperStrengthPower = (Button)view.findViewById(R.id.btnSuperStrengthPower);
        btnShowBackstory = (Button)view.findViewById(R.id.btnShowBackstory);

        btnTurtlePower.setOnClickListener(this);
        btnLightningPower.setOnClickListener(this);
        btnFlightPower.setOnClickListener(this);
        btnWebSlingingPower.setOnClickListener(this);
        btnLaserVisionPower.setOnClickListener(this);
        btnSuperStrengthPower.setOnClickListener(this);

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onPickPowerInteraction(uri);
        }
    }

    @Override
    public void onClick(View v) {
        btnShowBackstory.setEnabled(true);
        btnShowBackstory.getBackground().setAlpha(255);

        btnTurtlePower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.turtle_power, 0, 0, 0);
        btnLightningPower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.thors_hammer, 0, 0, 0);
        btnFlightPower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.super_man_crest, 0, 0, 0);
        btnWebSlingingPower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spider_web, 0, 0, 0);
        btnLaserVisionPower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.laser_vision, 0, 0, 0);
        btnSuperStrengthPower.setCompoundDrawablesWithIntrinsicBounds(R.drawable.super_strength, 0, 0, 0);

        Button btn = (Button)v;
        int leftDrawable = 0;
        int rightDrawable = R.drawable.item_selected;

        if (btn == btnTurtlePower) {
            leftDrawable = R.drawable.turtle_power;
        } else if (btn == btnLightningPower) {
            leftDrawable = R.drawable.thors_hammer;
        } else if (btn == btnFlightPower) {
            leftDrawable = R.drawable.super_man_crest;
        } else if (btn == btnWebSlingingPower) {
            leftDrawable = R.drawable.spider_web;
        } else if (btn == btnLaserVisionPower) {
            leftDrawable = R.drawable.laser_vision;
        } else if (btn == btnSuperStrengthPower) {
            leftDrawable = R.drawable.super_strength;
        }

        btn.setCompoundDrawablesWithIntrinsicBounds(leftDrawable, 0, rightDrawable, 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PickPowerInteractionListener) {
            mListener = (PickPowerInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface PickPowerInteractionListener {
        // TODO: Update argument type and name
        void onPickPowerInteraction(Uri uri);
    }
}
