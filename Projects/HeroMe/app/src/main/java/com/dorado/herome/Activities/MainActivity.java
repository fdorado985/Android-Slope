package com.dorado.herome.Activities;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dorado.herome.Fragments.MainFragment;
import com.dorado.herome.Fragments.PickPowerFragment;
import com.dorado.herome.R;

public class MainActivity extends AppCompatActivity implements MainFragment.MainFragmentInteractionListener, PickPowerFragment.PickPowerInteractionListener {

    // View cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new MainFragment();

            /*FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.fragment_container, fragment);
            transaction.commit();*/

            manager.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }

    // MainFragmentInteraction Listener
    @Override
    public void onMainFragmentInteraction(Uri uri) {

    }

    // PickPowerInteraction Listener
    @Override
    public void onPickPowerInteraction(Uri uri) {

    }

    // Functions

    public void loadPickPowerScreen() {
        PickPowerFragment pickPowerFragment = new PickPowerFragment();
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, pickPowerFragment)
                .addToBackStack(null)
                .commit();
    }
}
