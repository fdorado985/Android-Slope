# HERO ME
This basic app works with two designs but what makes it special is...
Where are replacing the use of `Activities` to use `Fragments`
You will see...
* How to call a `Fragment`
* How add `drawables` to the left|right of buttons
* Use a main `toolbar` for all `fragments`
* User interaction inside `fragments`
* Replace a `fragment` with other to change `screen`.

## Demo
![hero_me_demo](.screenshots/hero_me_demo.gif)
