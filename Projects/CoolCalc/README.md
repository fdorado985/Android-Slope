# CoolCalc
This is a simple basic calculator to see how to use `LinearLayouts`, `weight_sum` | `layout_weight` to divide the screen space using `layout_height` to be vertical or `layout_widht` to be horizontal.
How to call `onClick` functions to execute processes, and reference of the `views` inside the `Java` class.

## Demo
![cool_calc_demo](.screenshots/cool_calc_demo.gif)
