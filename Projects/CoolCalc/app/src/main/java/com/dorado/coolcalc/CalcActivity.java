package com.dorado.coolcalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CalcActivity extends Activity {

  // Views
  TextView txtResult;

  // Properties
  String currentValue = "";
  String leftValue = "";
  String rightValue = "";
  int result = 0;
  Operation currentOperation;

  // Enum
  public enum Operation {
    ADD, SUBTRACT, DIVIDE, MULTIPLY, EQUAL
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_calc);

    // Text Result
    txtResult = (TextView) findViewById(R.id.txtResult);
    txtResult.setText("");
  }

  public void onBtnZeroTapped(View view) {
    numberPressed(0);
  }

  public void onBtnOneTapped(View view) {
    numberPressed(1);
  }

  public void onBtnTwoTapped(View view) {
    numberPressed(2);
  }

  public void onBtnThreeTapped(View view) {
    numberPressed(3);
  }

  public void onBtnFourTapped(View view) {
    numberPressed(4);
  }

  public void onBtnFiveTapped(View view) {
    numberPressed(5);
  }

  public void onBtnSixTapped(View view) {
    numberPressed(6);
  }

  public void onBtnSevenTapped(View view) {
    numberPressed(7);
  }

  public void onBtnEightTapped(View view) {
    numberPressed(8);
  }

  public void onBtnNineTapped(View view) {
    numberPressed(9);
  }

  public void onBtnClearTapped(View view) {
    leftValue = "";
    rightValue = "";
    result = 0;
    currentValue = "";
    currentOperation = null;
    txtResult.setText("0");
  }

  public void onBtnCalculateTapped(View view) {
    processOperation(Operation.EQUAL);
  }

  public void onBtnAddTapped(View view) {
    processOperation(Operation.ADD);
  }

  public void onBtnSubtractTapped(View view) {
    processOperation(Operation.SUBTRACT);
  }

  public void onBtnDivideTapped(View view) {
    processOperation(Operation.DIVIDE);
  }

  public void onBtnMultiplyTapped(View view) {
    processOperation(Operation.MULTIPLY);
  }

  void numberPressed(int number) {
    currentValue += String.valueOf(number);
    txtResult.setText(currentValue);
  }

  void processOperation(Operation operation) {
    if (currentOperation != null) {
      if (currentValue != "") {
        rightValue = currentValue;
        currentValue = "";

        switch (currentOperation) {
          case ADD:
            result = Integer.parseInt(leftValue) + Integer.parseInt(rightValue);
            break;
          case SUBTRACT:
            result = Integer.parseInt(leftValue) - Integer.parseInt(rightValue);
            break;
          case MULTIPLY:
            result = Integer.parseInt(leftValue) * Integer.parseInt(rightValue);
            break;
          case DIVIDE:
            result = Integer.parseInt(leftValue) / Integer.parseInt(rightValue);
            break;
        }

        leftValue = String.valueOf(result);
        txtResult.setText(leftValue);
      }
    } else {
      leftValue = currentValue;
      currentValue = "";
    }

    currentOperation = operation;
  }
}