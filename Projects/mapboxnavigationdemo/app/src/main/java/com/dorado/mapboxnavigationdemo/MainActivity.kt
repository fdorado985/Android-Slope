package com.dorado.mapboxnavigationdemo

import android.annotation.SuppressLint
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.mapbox.android.core.location.LocationEngine
import com.mapbox.android.core.location.LocationEngineListener
import com.mapbox.android.core.location.LocationEnginePriority
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), PermissionsListener, LocationEngineListener, MapboxMap.OnMapClickListener {

  // Properties

  private lateinit var mapView: MapView
  private lateinit var map: MapboxMap
  private lateinit var btnStart: Button
  private lateinit var permissionManager: PermissionsManager
  private lateinit var originLocation: Location
  private lateinit var originPosition: Point
  private lateinit var destinationPosition: Point
  private lateinit var currentRoute: DirectionsRoute

  private var locationEngine: LocationEngine? = null
  private var locationLayerPlugin: LocationLayerPlugin? = null
  private var destinationMarker: Marker? = null
  private var navigationMapRoute: NavigationMapRoute? = null

  // View cycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    Mapbox.getInstance(applicationContext, getString(R.string.access_token))
    mapView = findViewById(R.id.mapView)
    btnStart = findViewById(R.id.btnStart)
    mapView.onCreate(savedInstanceState)
    mapView.getMapAsync { mapboxMap ->
      map = mapboxMap
      map.addOnMapClickListener(this)
      enableLocation()
    }

    btnStart.setOnClickListener {
      val options = NavigationLauncherOptions
          .builder()
          .directionsRoute(currentRoute)
          .shouldSimulateRoute(true)
          .build()

      NavigationLauncher.startNavigation(this, options)
    }
  }

  override fun onStart() {
    super.onStart()
    mapView.onStart()
    if (PermissionsManager.areLocationPermissionsGranted(this)) {
      locationEngine?.requestLocationUpdates()
      locationLayerPlugin?.onStart()
    }
  }

  override fun onResume() {
    super.onResume()
    mapView.onResume()
  }

  override fun onPause() {
    super.onPause()
    mapView.onPause()
  }

  override fun onStop() {
    super.onStop()
    mapView.onStop()
    locationEngine?.removeLocationUpdates()
    locationLayerPlugin?.onStop()
  }

  override fun onDestroy() {
    super.onDestroy()
    mapView.onDestroy()
    locationEngine?.deactivate()
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    super.onSaveInstanceState(outState)
    if (outState != null) {
      mapView.onSaveInstanceState(outState)
    }
  }

  override fun onLowMemory() {
    super.onLowMemory()
    mapView.onLowMemory()
  }

  // Private

  private fun enableLocation() {
    if (PermissionsManager.areLocationPermissionsGranted(this)) {
      initializeLocationEngine()
      initializeLocationLayer()
    } else {
      permissionManager = PermissionsManager(this)
      permissionManager.requestLocationPermissions(this)
    }
  }

  @SuppressLint("MissingPermission")
  private fun initializeLocationEngine() {
    locationEngine = LocationEngineProvider(this).obtainBestLocationEngineAvailable()
    locationEngine?.priority = LocationEnginePriority.HIGH_ACCURACY
    locationEngine?.activate()

    val lastLocation = locationEngine?.lastLocation
    if (lastLocation != null) {
      originLocation = lastLocation
      setCameraPosition(lastLocation)
    } else {
      locationEngine?.addLocationEngineListener(this)
    }
  }

  @SuppressLint("MissingPermission")
  private fun initializeLocationLayer() {
    locationLayerPlugin = LocationLayerPlugin(mapView, map, locationEngine)
    locationLayerPlugin?.isLocationLayerEnabled = true
    locationLayerPlugin?.cameraMode = CameraMode.TRACKING
    locationLayerPlugin?.renderMode = RenderMode.NORMAL

  }

  private fun setCameraPosition(location: Location) {
    map.animateCamera(CameraUpdateFactory.newLatLngZoom(
        LatLng(location.latitude, location.longitude), 13.0))
  }

  private fun getRoute(origin: Point, destination: Point) {
    NavigationRoute.builder(this)
        .accessToken(Mapbox.getAccessToken()!!)
        .origin(origin)
        .destination(destination)
        .build()
        .getRoute(object : Callback<DirectionsResponse> {

          override fun onResponse(call: Call<DirectionsResponse>, response: Response<DirectionsResponse>) {
            val routeResponse = response ?: return
            val body = routeResponse.body() ?: return
            if (body.routes().count() == 0) {
              Log.e("MainActivity", "No route found.")
              return
            }

            currentRoute = body.routes().first()
            navigationMapRoute = NavigationMapRoute(null, mapView, map)
            navigationMapRoute?.addRoute(currentRoute)
          }

          override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
            Log.e("MainActivity", "ErrirL ${t?.localizedMessage}")
            return
          }
        })
  }

  // Listeners

  override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
    // Present a toast or a dialog explaining why they need to grant access
  }

  override fun onPermissionResult(granted: Boolean) {
    if (granted) {
      enableLocation()
    }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
  }

  override fun onLocationChanged(location: Location?) {
    location?.let {
      originLocation = location
      setCameraPosition(location)
    }
  }

  @SuppressLint("MissingPermission")
  override fun onConnected() {
    locationEngine?.requestLocationUpdates()
  }

  override fun onMapClick(point: LatLng) {
    destinationMarker.let {
      if (it != null) {
        map.removeMarker(it)
      }
    }

    destinationMarker = map.addMarker(MarkerOptions().position(point))
    destinationPosition = Point.fromLngLat(point.longitude, point.latitude)
    originPosition = Point.fromLngLat(originLocation.longitude, originLocation.latitude)
    getRoute(originPosition, destinationPosition)

    btnStart.isEnabled = true
    btnStart.setBackgroundResource(R.color.mapbox_blue)
  }
}
