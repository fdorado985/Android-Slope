#  Mapbox Navigation
On this simple app you will see in less than 20 lines of code how to use Mapbox to create a map, follow a route, create a line to connect an origin with a destination, how to zoom and track the user movement, also create a navigation to let you know the route while you are on the way.

> This version has been developed on Java... if you want to take a look to the `JAVA` version... take a look at here... [Mapbox_Java](../MapboxJavaDemo)

## Demo
![mapbox_demo](.screenshots/mapbox_navigation_demo.gif)
