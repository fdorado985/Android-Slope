package com.example.juandorado.bootcamplocator.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juandorado.bootcamplocator.R;
import com.example.juandorado.bootcamplocator.holders.LocationsViewHolder;
import com.example.juandorado.bootcamplocator.model.Devslopes;

import java.util.ArrayList;

public class LocationsAdapter extends RecyclerView.Adapter<LocationsViewHolder> {

    // Properties

    private ArrayList<Devslopes> locations;

    // Constructor

    public LocationsAdapter(ArrayList<Devslopes> locations) {
        this.locations = locations;
    }

    // RecyclerView.Adapter

    @NonNull
    @Override
    public LocationsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View card = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_location, viewGroup, false);
        return new LocationsViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationsViewHolder locationsViewHolder, int i) {
        final Devslopes location = locations.get(i);
        locationsViewHolder.updateUI(location);

        locationsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Load details page
            }
        });
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }
}
