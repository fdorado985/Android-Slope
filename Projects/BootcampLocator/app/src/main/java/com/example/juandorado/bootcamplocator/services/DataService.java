package com.example.juandorado.bootcamplocator.services;

import com.example.juandorado.bootcamplocator.model.Devslopes;

import java.util.ArrayList;

public class DataService {
    private static final DataService ourInstance = new DataService();

    public static DataService getInstance() {
        return ourInstance;
    }

    private DataService() { }

    public ArrayList<Devslopes> getBootcampLocationsWithin10MilesOfZip(int zipcode) {
        // Pretending we are downloading data from the server

        ArrayList<Devslopes> list = new ArrayList<Devslopes>();
        list.add(new Devslopes(35.279f,-120.663f,"Downtown","762 Higuera Street, San Luis Obispo, CA 93401","slo"));
        list.add(new Devslopes(35.302f,-120.658f,"On The Campus","1 Grand Ave, San Luis Obispo, CA 93401","slo"));
        list.add(new Devslopes(35.267f,-120.652f,"East Side Tower","2494 Victoria Ave, San Luis Obispo, CA 93401","slo"));
        return list;
    }
}
