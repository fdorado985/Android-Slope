package com.example.juandorado.bootcamplocator.holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juandorado.bootcamplocator.R;
import com.example.juandorado.bootcamplocator.model.Devslopes;

public class LocationsViewHolder extends RecyclerView.ViewHolder {

    // Properties

    private ImageView imgLocation;
    private TextView txtLocationTitle;
    private TextView txtLocationAddress;

    // Constructor

    public LocationsViewHolder(@NonNull View itemView) {
        super(itemView);

        imgLocation = itemView.findViewById(R.id.imgLocation);
        txtLocationTitle = itemView.findViewById(R.id.txtLocationTitle);
        txtLocationAddress = itemView.findViewById(R.id.txtLocationAddress);
    }

    // Functions

    public void updateUI(Devslopes location) {
        String uri = location.getLocationImgUrl();
        int resource = imgLocation.getResources().getIdentifier(uri, null, imgLocation.getContext().getPackageName());
        imgLocation.setImageResource(resource);
        txtLocationTitle.setText(location.getLocationTitle());
        txtLocationAddress.setText(location.getLocationAddress());
    }
}
