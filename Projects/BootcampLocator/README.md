# Bootcamp Locator
On this app you will see combining how are the `Fragments` used also `CardViews` and `Maps`

You are gonna see...
* How to `Show`|`Hide` a `Fragment`
* Create `Custom Fragments`
* Create `Markers` on Map
* Get a zip from a `Location`
* Ask for `Permissions`

## Demo
![bootcamp_locator_demo](.screenshots/bootcamp_locator_demo.gif)
