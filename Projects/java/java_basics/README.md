# Java Basics
On this project you will see a list of classes to learn the basics of Java... you will be able to see...

* Classic Hello World
* Variables
* Primitive Numbers
* Primitives Bool and Char
* Strings
* Methods
* Conditional Logic
* Arrays
* Java classes
* Java inheritance
* Java polymorphism
* Java data encapsulation.
