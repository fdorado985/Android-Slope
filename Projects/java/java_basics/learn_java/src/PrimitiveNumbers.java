public class PrimitiveNumbers {

    public static void main(String[] args) {
        // Whole Number Primitives
        byte numByte = 100; // 8 bit : -128 to 127
        short numShort = 1000; // 16 bit : -32768 to 32767
        short numShort2 = 32_767; // 16 bit : -32768 to 32767
        int numInt = 123_456_789; // 32 bit : -2_147_483_648 to 2_147_483_647, inclusive
        long numLong = 123_456_789_123L; // 64 bit

        // Decimal numbers
        float numFloat = 123.123456123f; // 32 bit : Single precision
        double numDouble = 123.123456123d; // 64 bit : Double precision - you don't need the 'd' at the end... it is a default type.

        System.out.println(numFloat);
        System.out.println(numDouble);

        // Math Operators +|-|*|/|%
        // int and double are the default types
        int sum = 5 + 10;
        double sumDecimals = 5.123 + 6.1293;
        short sum2 = (short)(5 + numByte);
    }
}
