public class Inheritance {

    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle("Honda", "Civic");
        vehicle.accelerate();
        vehicle.printDetails();

        Car car = new Car("Chevy", "Camaro");
        car.accelerate();
        car.printDetails();

        Bus bus = new Bus("Dodge", "Biggy");
        bus.accelerate();
        bus.printDetails();
    }
}

class Vehicle {

    private String make;
    private String model;
    protected int horsepower;

    public Vehicle(String make, String model) {
        this.make = make;
        this.model = model;
    }

    public void accelerate() {
        // Write some code to accelerate a vehicle
        System.out.println("Accelerating vehicle");
    }

    public void printDetails() {
        System.out.println("Make: " + this.make);
        System.out.println("Model: " + this.model);
    }
}

class Car extends Vehicle {

    public Car(String make, String model) {
        super(make, model);
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("Accelerating the car");
    }

    public void turnOnNOS() {
        this.horsepower += 100;
    }
}

class Bus extends Vehicle {

    public Bus(String make, String model) {
        super(make, model);
    }

    @Override
    public void accelerate() {
        //super.accelerate(); // This is optional
        System.out.println("Accelerating the bus");
    }
}
