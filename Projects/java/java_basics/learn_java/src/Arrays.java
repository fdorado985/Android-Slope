public class Arrays {

    public static void main(String[] args) {
        String name;
        name = "Juan";

        // Array initialization
        String[] myTrophyArray;
        myTrophyArray = new String[4];

        // Initialize in just one line
        String[] myTrophyArray2 = new String[4];

        myTrophyArray[0] = "First Place Trophy";
        myTrophyArray[1] = "Participation Ribbon";
        myTrophyArray[3] = "Second Place";

        System.out.println(myTrophyArray[0]);
        System.out.println(myTrophyArray[1]);
        System.out.println(myTrophyArray[2]);
        System.out.println(myTrophyArray[3]);

        int lenght = myTrophyArray.length;
        System.out.println("Last trophy " + myTrophyArray[lenght - 1]);

        int[] highScores = new int[5];
        highScores[0] = 500;
        highScores[1] = 400;
        highScores[2] = 300;
        highScores[3] = 200;
        highScores[4] = 100;

        System.out.println(highScores[0]);

        highScores[0] = 600;

        System.out.println(highScores[0]);

        int[] stockPrices = new int[] { 100, 1120, 123, 1234, 3948 };
    }
}
