public class Classes {

    public static void main(String[] args) {
        Person person = new Person("Jack", "Bauer", 45);
        person.speakName();
    }
}

class Person {

    // Properties

    private String firstName;
    private String lastName;
    private int age;

    // Constructor

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    // Functions

    public void speakName() {
        System.out.println("Hello, my name is: " + firstName + " " + lastName);
    }
}