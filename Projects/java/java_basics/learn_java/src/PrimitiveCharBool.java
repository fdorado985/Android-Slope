public class PrimitiveCharBool {

    public static void main(String[] args) {
        char myChar = 'A'; // It is only one character
        char unicode = '\u03A8'; // Char by unicode

        System.out.println(unicode);

        boolean myBool = true;
        boolean myFalseBool = false;
    }
}