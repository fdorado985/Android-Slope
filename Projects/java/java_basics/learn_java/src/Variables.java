public class Variables {

    public static void main(String[] args) {
        final String personName = "Juan Dorado";
        // personName = "Batman";
        Boolean isAwesome = true;
        int a = 3;
        int b = 6;
        // Common Error Types
        //String numberName = 3; // Error - Incompatible types

        System.out.println("Is " + personName + " awesome? - The answer is : " + isAwesome);
        System.out.println(a + b);
    }
}
