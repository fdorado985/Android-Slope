public class Strings {

    public static void main(String[] args) {
        String jediMaster = "Luke Skywalker";
        String lightSaberColor = "green";
        System.out.println(jediMaster);
        System.out.println(lightSaberColor);

        // Concatenate
        String jediDetails = jediMaster + " has a " + lightSaberColor + " lightsaber.";
        System.out.println(jediDetails);

        // Lenght | isEmpty
        String password = "password123";
        System.out.println(password.length());
        System.out.println(password.isEmpty());

        // Equals
        String passwordConfirm = "pasSword123";
        System.out.println(password.equals(passwordConfirm));
        System.out.println(password.equalsIgnoreCase(passwordConfirm));

        // Substrings
        String firstName = jediMaster.substring(0, 4);
        String lastName = jediMaster.substring(5);
        System.out.println(firstName);
        System.out.println(lastName);

        // Replacing
        String phone = "123-4567-1234";
        System.out.println(phone.replace("-", ""));

        // Contains
        System.out.println(jediMaster.contains("Skywalker"));
        System.out.println(jediMaster.contains("Qui Gon"));

        // Lower|Upper case
        System.out.println(jediMaster.toUpperCase());
        System.out.println(jediMaster.toLowerCase());
    }
}
