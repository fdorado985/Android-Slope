public class Methods {

    public static void main(String[] args) {
        printName();
        greeting("JuanD");
        calculateArea(2,2);

        int l = 5;
        int w = 10;
        calculateArea(l, w);

        int squareArea = calculateAndReturnArea(10, 10);
        System.out.println(squareArea);

        String name = getName();
        System.out.println(name);
    }

    // Function that does not receive or return anything
    public static void printName() {
        System.out.println("My name is JuanD");
    }

    // Method where we supply a parameter
    public static void greeting(String name) {
        System.out.println("Hello " + name);
    }

    // Method where we supply a couple of parameters
    public static void calculateArea(int lenght, int width) {
        int area = lenght * width;
        System.out.println(area);
    }

    // Method where we get something back from the method
    public static int calculateAndReturnArea(int lenght, int width) {
        int area = lenght * width;
        return area;
    }

    public static String getName() {
        String name = "JuanD";
        return name;
    }
}
