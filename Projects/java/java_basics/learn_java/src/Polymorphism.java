public class Polymorphism {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.calculateArea(20, 10);

        Triangle triangle = new Triangle();
        triangle.calculateArea(20, 10);
    }
}

class Shape {

    protected int area;

    public void calculateArea() {
        System.out.println("Rectangle Area: " + area);
    }

    public void calculateArea(int base, int height) {

    }
}

class Rectangle extends Shape {

    @Override
    public void calculateArea(int base, int height) {
        super.calculateArea(base, height);
        this.area = base * height;
        super.calculateArea();
    }
}

class Triangle extends Shape {

    @Override
    public void calculateArea(int base, int height) {
        super.calculateArea(base, height);
        this.area = (base * height) / 2;
        super.calculateArea();
    }
}
