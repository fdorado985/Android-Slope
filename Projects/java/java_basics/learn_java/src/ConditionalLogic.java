public class ConditionalLogic {

    public static void main(String[] args) {
        int a = 2;
        int b = 3;

        if (a == b) {
            // If true run in here
            System.out.println("Yes! A does equal B");
        }

        if (a != b) {
            System.out.println("No!, A does not equal B");
        }

        int accountBalance = 100;
        int price = 50;

        if (accountBalance >= price) {
            System.out.println("Treat yo self. You can buy this!");
        } else {
            System.out.println("Sorry, you're broke");
        }

        int degrees = 49;

        if (degrees >= 70) {
            System.out.println("This is a some nice weather");
        } else if (degrees < 70 && degrees >= 50) {
            System.out.println("Grab a sweater");
        } else {
            System.out.println("Holy crap its cold!");
        }

        boolean isHungry = true;
        boolean isBored = false;

        if (isHungry == true) {
            System.out.println("Lets get some pizza!");
        }

        if (isHungry) {
            System.out.println("Lets get some pizza!");
        }
    }
}
