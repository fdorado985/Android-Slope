#  Dinner Decider

This is a basic app, to choose between a list of food, for those times that nobody decides what to eat.

## Demo
![demo_dinner_decider](screenshots/dinner_decider_demo.gif)
