package com.dorado.dinnerdecider

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

  private val foodList = arrayListOf("Chinese", "Hamburger", "Pizza", "McDonalds", "Barros Pizza")

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    decideBtn.setOnClickListener {
      val random = Random()
      val randomFoodIndex = random.nextInt(foodList.count())
      selectedFoodText.text = foodList[randomFoodIndex]
    }

    addFoodBtn.setOnClickListener {
      val newFood = addFoodTxt.text.toString()
      if (newFood.isNotEmpty()) {
        foodList.add(newFood)
      }
      addFoodTxt.text.clear()
    }
  }
}
