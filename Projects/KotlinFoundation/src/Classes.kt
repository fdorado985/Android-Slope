//class Car constructor(val make: String, val model: String, var color: String) {
//
//    fun accelerate() {
//        println("vrooom vrooom")
//    }
//
//    fun details() {
//        println("This is a $color $make $model")
//    }
//}
//
//class Truck(val make: String, val model: String, val towingCapacity: Int) {
//
//    fun tow() {
//        println("taking the horses to the rodeo")
//    }
//
//    fun details() {
//        println("The $make $model has a towing capacity of $towingCapacity")
//    }
//}

open class Vehicle(val make: String, val model: String) {

    open fun accelerate() {
        println("vrooom vrooom")
    }

    fun park() {
        println("parking the vehicle")
    }

    fun brake() {
        println("STOP")
    }
}

class Car(make: String, model: String, color: String): Vehicle(make, model) {

    override fun accelerate() {
        super.accelerate()
        println("We are going lucrous mode!")
    }
}

class Truck(make: String, model: String): Vehicle(make, model) {

    fun tow() {
        println("headed out to the mountains!")
    }
}

class SportCar(make: String, model: String) : Vehicle(make, model)

fun main(args: Array<String>) {
//    val car = Car("Toyota", "Avalon", "Red")
//    println(car.make)
//    println(car.model)
//    car.accelerate()
//
//    val truck = Truck("Ford", "F-150", 1000)
//    truck.tow()
//    truck.details()

    val tesla = Car("Tesla", "ModelS", "Red")
    tesla.accelerate()
}