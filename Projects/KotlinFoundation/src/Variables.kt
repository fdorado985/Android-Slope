fun main(args: Array<String>) {
    val name: String = "JonnyD"
    var isAwesome = true
    isAwesome = false

    println("Is " + name + " awesome? The answer is: " + isAwesome)

    var a = 3
    var b = 6
    print(a + b)

    val doubleNum = 123.45 // 64 bit number
    val floatNum = 123.45f // 32 bit
    val longNum = 123456789L // 64 bit

    val aDouble = a.toString()

    var hero: String = "Batman"
    println(hero)
}