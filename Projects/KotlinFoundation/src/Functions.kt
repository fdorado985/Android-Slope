// FUNCTIONS

fun main(args: Array<String>) {

    fun forceChoke() {
        println("You have failed me for the las time Admiral...")
    }

    forceChoke()

    fun makeAnEntrance(line: String) {
        println(line)
    }

    makeAnEntrance("I find your lack of faith disturbing")

    fun calculateNumberGoodGuys(rebels: Int, ewoks: Int): Int {
        val goodGuys = rebels + ewoks
        return goodGuys
    }

    val rebels = calculateNumberGoodGuys(8, 12)
    println(rebels)
    println("Darth Vader faced off against ${rebels} rebel scum")
    println("Darth Vader faced off against ${calculateNumberGoodGuys(8, 12)} rebel scum")

    fun vaderIsFeeling(mood: String = "angry") {
        println(mood)
    }

    vaderIsFeeling()
    vaderIsFeeling("Happy")
}