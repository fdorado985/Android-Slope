// LOOPS

fun main(args: Array<String>) {

    val rebels = arrayListOf<String>("Leia", "Luke", "Han Solo", "Mon Mothhma")
    val rebelVehicles = hashMapOf<String, String>("Solo" to "Millenium Falcon", "Luke" to "Landspeeder", "Boba Fett" to "Rocket Pack")

    for (rebel in rebels) {
        println("Name: $rebel")
    }

    for ((key, vehicle) in rebelVehicles) {
        println("$key gets around in their $vehicle")
    }

    var x = 10
    while (x > 0) {
        println(x)
        x--
    }
}