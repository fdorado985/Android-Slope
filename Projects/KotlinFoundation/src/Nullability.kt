// NULLABILTIY

fun main(args: Array<String>) {
    var name: String = "JonnyD"
    // name = null

    var nullableName: String? = "Do i really exist?"
    nullableName = null

    println(nullableName?.length)

    // Null Check

    var lenght = 0
    if (nullableName != null) {
        lenght = nullableName.length
    } else {
        lenght = -1
    }
    println(lenght)

    val l = if (nullableName != null) nullableName.length else -1
    print(l)

    // Second method Safe Call Operator ?

    println(nullableName?.length)

    // Third method is Elvis Operator

    val len = nullableName?.length ?: -1
    val noName = nullableName ?: "No knows me..."

    println(noName)

    // !!

    println(nullableName!!.length)
}