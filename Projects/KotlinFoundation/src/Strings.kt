fun main(args: Array<String>) {
    val str = "May the force be with you"
    println(str)

    val str1 = "My dad said the funniest thing he said \"A joke\""
    println(str1)

    val rawCrawl = """|A long time ago,
    |in a galaxy
    |far, far away...
    |BUMM, BUMMM, BUMMMM""".trimMargin()
    println(rawCrawl)

    for (char in str) {
        print(char)
    }

    val contentEquals = str.contentEquals("May the force be with you")
    println(contentEquals)

    val contains = str.contains("force", true)
    println(contains)

    val upperCase = str.toUpperCase()
    val lowerCase = str.toLowerCase()
    println(upperCase)
    println(lowerCase)

    val num = 6
    val stringNum = num.toString()
    println(stringNum)

    val subsequence = str.subSequence(4, 13)
    println(subsequence)

    val luke = "Luke Skywalker"
    val lightSaberColor = "green"
    val vehicle = "landspeeder"
    println(luke + " has a " + lightSaberColor + " saber")
    println("$luke has a ${lightSaberColor.toUpperCase()} saber color and drives a $vehicle")
}