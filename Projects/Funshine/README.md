# Funshine

Here you will see how to do `Web Requests` & `REST`, we are gonna be using `Volley` to help ourselves to call services and how to `Parse` from a `JSON`.

## Demo
![funshine_demo](.screenshots/funshine_demo.png)
