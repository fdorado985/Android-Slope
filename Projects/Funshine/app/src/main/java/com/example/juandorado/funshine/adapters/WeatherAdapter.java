package com.example.juandorado.funshine.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juandorado.funshine.R;
import com.example.juandorado.funshine.holders.WeatherReportViewHolder;
import com.example.juandorado.funshine.model.DailyWeatherReport;

import java.util.ArrayList;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherReportViewHolder> {

    private ArrayList<DailyWeatherReport> mDailyWeatherReport;

    public WeatherAdapter(ArrayList<DailyWeatherReport> mDailyWeatherReport) {
        this.mDailyWeatherReport = mDailyWeatherReport;
    }

    @NonNull
    @Override
    public WeatherReportViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View card = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_weather, viewGroup, false);
        return new WeatherReportViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherReportViewHolder weatherReportViewHolder, int i) {
        DailyWeatherReport report = mDailyWeatherReport.get(i);
        weatherReportViewHolder.updateUI(report);
    }

    @Override
    public int getItemCount() {
        return mDailyWeatherReport.size();
    }
}
