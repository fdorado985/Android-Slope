package com.example.juandorado.funshine.holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juandorado.funshine.R;
import com.example.juandorado.funshine.model.DailyWeatherReport;

public class WeatherReportViewHolder extends RecyclerView.ViewHolder {

    private ImageView imgWeatherType;
    private TextView lblDate;
    private TextView lblWeatherType;
    private TextView lblMaxTemp;
    private TextView lblMinTemp;

    public WeatherReportViewHolder(@NonNull View itemView) {
        super(itemView);

        imgWeatherType = itemView.findViewById(R.id.img_weather_type);
        lblDate = itemView.findViewById(R.id.lbl_weather_date);
        lblWeatherType = itemView.findViewById(R.id.lbl_weather_type);
        lblMaxTemp = itemView.findViewById(R.id.lbl_weather_max_temp);
        lblMinTemp = itemView.findViewById(R.id.lbl_weather_min_temp);
    }

    public void updateUI(DailyWeatherReport report) {
            switch (report.getWeather()) {
                case DailyWeatherReport.WEATHER_TYPE_CLOUDS:

                    imgWeatherType.setImageDrawable(imgWeatherType.getResources().getDrawable(R.drawable.cloudy_mini));
                    break;
                case DailyWeatherReport.WEATHER_TYPE_RAIN:
                    imgWeatherType.setImageDrawable(imgWeatherType.getResources().getDrawable(R.drawable.rainy_mini));
                    break;
                case DailyWeatherReport.WEATHER_TYPE_SNOW:
                    imgWeatherType.setImageDrawable(imgWeatherType.getResources().getDrawable(R.drawable.snow_mini));
                    break;
                case DailyWeatherReport.WEATHER_TYPE_WIND:
                    imgWeatherType.setImageDrawable(imgWeatherType.getResources().getDrawable(R.drawable.thunder_lightning_mini));
                    break;
                default:
                    imgWeatherType.setImageDrawable(imgWeatherType.getResources().getDrawable(R.drawable.sunny_mini));
                    break;
            }

            lblDate.setText(report.getFormattedDate());
            lblMaxTemp.setText(Integer.toString(report.getCurrentTemp()) + "°");
            lblMinTemp.setText(Integer.toString(report.getMinTemp()) + "°");
            lblWeatherType.setText(report.getWeather());
        }
}
