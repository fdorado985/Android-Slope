package com.example.juandorado.instaslam.model;

import android.net.Uri;

public class InstaImage {

    // Properties

    private Uri imgResourceUrl;

    public Uri getImgResourceUrl() {
        return imgResourceUrl;
    }

    // Constructor

    public InstaImage(Uri imgResourceUrl) {
        this.imgResourceUrl = imgResourceUrl;
    }
}
