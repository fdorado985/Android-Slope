package com.dorado.fitorflab;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    // View cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        TextView txtExercise = (TextView)findViewById(R.id.txtExercise);
        ImageView imgExercise = (ImageView) findViewById(R.id.imgExercise);
        ConstraintLayout mainBg = (ConstraintLayout) findViewById(R.id.mainBG);

        String exerciseTitle = getIntent().getStringExtra(MainActivity.EXTRA_ITEM_TITLE);
        txtExercise.setText(exerciseTitle);

        if (exerciseTitle.equalsIgnoreCase(MainActivity.EXERCISE_WEIGHTS)) {
            imgExercise.setImageDrawable(getResources().getDrawable(R.drawable.weight, getApplicationContext().getTheme()));
            mainBg.setBackgroundColor(Color.parseColor("#2CA5F5"));

        } else if (exerciseTitle.equalsIgnoreCase(MainActivity.EXERCISE_YOGA)) {
            imgExercise.setImageDrawable(getResources().getDrawable(R.drawable.lotus, getApplicationContext().getTheme()));
            mainBg.setBackgroundColor(Color.parseColor("#916BCD"));

        } else if (exerciseTitle.equalsIgnoreCase(MainActivity.EXERCISE_CARDIO)) {
            imgExercise.setImageDrawable(getResources().getDrawable(R.drawable.heart, getApplicationContext().getTheme()));
            mainBg.setBackgroundColor(Color.parseColor("#52AD56"));

        }
    }
}
