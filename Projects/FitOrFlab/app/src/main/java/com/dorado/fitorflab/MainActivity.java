package com.dorado.fitorflab;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    // Constants

    public static final String EXTRA_ITEM_TITLE = "extra.item.title";
    public static final String EXERCISE_WEIGHTS = "Weight lifting";
    public static final String EXERCISE_YOGA = "Yoga";
    public static final String EXERCISE_CARDIO = "Cardio";

    // View cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConstraintLayout weightBtn = (ConstraintLayout)findViewById(R.id.weight_layout);
        ConstraintLayout yogaBtn = (ConstraintLayout)findViewById(R.id.yoga_layout);
        ConstraintLayout cardioBtn = (ConstraintLayout)findViewById(R.id.cardio_layout);

        weightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDetailActivity(MainActivity.EXERCISE_WEIGHTS);
            }
        });

        yogaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDetailActivity(MainActivity.EXERCISE_YOGA);
            }
        });

        cardioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDetailActivity(MainActivity.EXERCISE_CARDIO);
            }
        });
    }

    // Functions

    private void loadDetailActivity(String exerciseTitle) {
        Intent detailsActivity = new Intent(MainActivity.this, DetailsActivity.class);
        detailsActivity.putExtra(MainActivity.EXTRA_ITEM_TITLE, exerciseTitle);
        startActivity(detailsActivity);
    }
}
