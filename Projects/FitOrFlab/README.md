# Fit Or Flab
On this app you will see how to pass data between one activity to another.

* `Activities`
* `Extras`
* `Resource` Images
* `Parse` colors
* `Constraint Layout`

## Demo
![fit_or_flab_demo](.screenshots/fit_or_flab_demo.gif)
