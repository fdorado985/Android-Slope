package com.dorado.swoosh.Controller

import android.os.Bundle
import com.dorado.swoosh.Model.Player
import com.dorado.swoosh.R
import com.dorado.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_finish.*

class FinishActivity : BaseActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_finish)

    val player = intent.getParcelableExtra<Player>(EXTRA_PLAYER)

    txtSearchLeagues.text = "Looking for ${player.league} ${player.skill} league near you..."
  }
}
