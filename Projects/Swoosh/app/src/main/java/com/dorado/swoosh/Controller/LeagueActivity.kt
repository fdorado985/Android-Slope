package com.dorado.swoosh.Controller

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.dorado.swoosh.Model.Player
import com.dorado.swoosh.R
import com.dorado.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : BaseActivity() {

  var player = Player("", "")

  override fun onSaveInstanceState(outState: Bundle?) {
    super.onSaveInstanceState(outState)
    outState?.putParcelable(EXTRA_PLAYER, player)
  }

  override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
    super.onRestoreInstanceState(savedInstanceState)
    if (savedInstanceState != null) {
      player = savedInstanceState?.getParcelable(EXTRA_PLAYER)
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_league)
  }

  fun btnLeagueNextTapped(view: View) {
    if (player.league != "") {
      val skillActivity = Intent(this, SkillActivity::class.java)
      skillActivity.putExtra(EXTRA_PLAYER, player)
      startActivity(skillActivity)
    } else {
      Toast.makeText(this, "Please select a league", Toast.LENGTH_SHORT).show()
    }
  }

  fun btnMenLeagueTapped(view: View) {
    btnWomenLeague.isChecked = false
    btnCoEdLeague.isChecked = false

    player.league = "men"
  }

  fun btnWomenLeagueTapped(view: View) {
    btnMenLeague.isChecked = false
    btnCoEdLeague.isChecked = false

    player.league = "women"
  }

  fun btnCoEdLeagueTapped(view: View) {
    btnMenLeague.isChecked = false
    btnWomenLeague.isChecked = false

    player.league = "co-ed"
  }
}
