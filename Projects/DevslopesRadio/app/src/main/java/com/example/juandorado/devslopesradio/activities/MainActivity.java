package com.example.juandorado.devslopesradio.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.juandorado.devslopesradio.R;
import com.example.juandorado.devslopesradio.fragments.DetailsFragment;
import com.example.juandorado.devslopesradio.fragments.MainFragment;
import com.example.juandorado.devslopesradio.model.Station;

public class MainActivity extends AppCompatActivity {

    // Properties

    private static MainActivity mainActivity;

    // Encapsulation

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    public static void setMainActivity(MainActivity mainActivity) {
        MainActivity.mainActivity = mainActivity;
    }

    // View cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity.setMainActivity(this);

        // Use the Fragment
        // Create the manager to handle the fragment
        FragmentManager manager = getSupportFragmentManager();
        // Initiate the fragment we want to show
        MainFragment mainFragment = (MainFragment)manager.findFragmentById(R.id.container_main);

        // Verify if the fragment is not null
        if (mainFragment == null) {
            // Initialize the fragment with the instanceMethod inside of it.
            mainFragment = MainFragment.newInstance("blah", "kah");
            // We start to show the fragment
            manager.beginTransaction().add(R.id.container_main, mainFragment).commit();
        }
    }

    // Functions

    public void loadDetailsScreen(Station selectedStation) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_main, new DetailsFragment())
                .addToBackStack(null)
                .commit();
    }
}
