package com.example.juandorado.devslopesradio.holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juandorado.devslopesradio.R;
import com.example.juandorado.devslopesradio.model.Station;

public class StationViewHolder extends RecyclerView.ViewHolder {

    // Properties

    private ImageView mainImage;
    private TextView txtTitle;

    // RecyclerView.ViewHolder Constructor
    public StationViewHolder(@NonNull View itemView) {
        super(itemView);

        this.mainImage = (ImageView)itemView.findViewById(R.id.main_image);
        this.txtTitle = (TextView)itemView.findViewById(R.id.main_text);
    }

    public void updateUI(Station station) {
        String uri = station.getImgUri();
        int resource = mainImage.getResources().getIdentifier(uri, null, mainImage.getContext().getPackageName());
        mainImage.setImageResource(resource);

        txtTitle.setText(station.getTitle());
    }
}
