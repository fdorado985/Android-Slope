package com.example.juandorado.devslopesradio.model;

public class Station {

    final String DRAWABLE = "drawable/";

    private String title;
    private String imgUri;

    public String getTitle() {
        return title;
    }

    public String getImgUri() {
        return DRAWABLE + imgUri;
    }

    public Station(String title, String imgUri) {
        this.title = title;
        this.imgUri = imgUri;
    }
}
