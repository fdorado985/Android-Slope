package com.example.juandorado.devslopesradio.services;

import com.example.juandorado.devslopesradio.model.Station;

import java.util.ArrayList;

public class DataService {
    private static final DataService ourInstance = new DataService();

    public static DataService getInstance() {
        return ourInstance;
    }

    private DataService() {

    }

    public ArrayList<Station> getFeaturedStations() {
        // Pretend we just downloaded featured stations from the internet.
        ArrayList<Station> list = new ArrayList<Station>();

        list.add(new Station("Flight Plan (Tunes for Travel)", "flightplanmusic"));
        list.add(new Station("Two-Wheelin (Biking Playlist)", "bicyclemusic"));
        list.add(new Station("Kids Jams (Music for Children)", "kidsmusic"));

        return list;
    }

    public ArrayList<Station> getRecentStations() {
        // Pretend we just downloaded featured stations from the internet.
        ArrayList<Station> list = new ArrayList<Station>();

        list.add(new Station("Vinyl Music (The 80's come back)", "vinylmusic"));
        list.add(new Station("Social (Friends playlist)", "socialmusic"));
        list.add(new Station("Main Music (All to begin)", "keymusic"));

        return list;
    }

    public ArrayList<Station> getPartyStations() {
        // Pretend we just downloaded featured stations from the internet.
        ArrayList<Station> list = new ArrayList<Station>();

        list.add(new Station("Flight Plan (Tunes for Travel)", "flightplanmusic"));
        list.add(new Station("Two-Wheelin (Biking Playlist)", "bicyclemusic"));
        list.add(new Station("Kids Jams (Music for Children)", "kidsmusic"));

        return list;
    }
}
