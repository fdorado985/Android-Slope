package com.example.juandorado.devslopesradio.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.juandorado.devslopesradio.R;
import com.example.juandorado.devslopesradio.activities.MainActivity;
import com.example.juandorado.devslopesradio.holders.StationViewHolder;
import com.example.juandorado.devslopesradio.model.Station;

import java.util.ArrayList;

public class StationsAdapter extends RecyclerView.Adapter<StationViewHolder> {

    private ArrayList<Station> stations;

    public StationsAdapter(ArrayList<Station> stations) {
        this.stations = stations;
    }

    // RecyclerView.Adapter<StationViewHolder>

    @NonNull
    @Override
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View stationCard = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_station, viewGroup, false);
        return new StationViewHolder(stationCard);
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder stationViewHolder, int i) {
        final Station station = stations.get(i);
        stationViewHolder.updateUI(station);

        stationViewHolder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Load details screen
                MainActivity.getMainActivity().loadDetailsScreen(station);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }
}
