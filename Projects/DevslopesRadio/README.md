# Devslopes Radio
This app covers the previous examples, on here we are going to see how work with a `Main Activity` with `Fragments` that will hold other `Fragments` at the time we use `Grids` and `List` inside a `RecyclerView`.

Learn how is the `RecyclerView` behaviour, how does it work with an `Adapter` and a `ViewHolder`.

## Demo
![devslopes_radio_demo](.screenshots/devslopes_radio_demo.gif)
