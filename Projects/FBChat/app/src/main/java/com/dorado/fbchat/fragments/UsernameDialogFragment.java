package com.dorado.fbchat.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.dorado.fbchat.R;
import com.dorado.fbchat.activities.ChatActivity;
import com.dorado.fbchat.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class UsernameDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.fragment_username_dialog, null))
                // Add action buttons
                .setPositiveButton(R.string.action_register, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText txtUsername = ((AlertDialog) dialog).findViewById(R.id.username);
                        String username = txtUsername.getText().toString();
                        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        User aUser = new User(username, "Empty", "Empty");
                        FirebaseDatabase.getInstance().getReference("users").child(userId).child("profile").setValue(aUser);

                        Intent intent = new Intent(getActivity().getBaseContext(), ChatActivity.class);
                        startActivity(intent);
                    }
                });
        return builder.create();
    }
}
