# FB Chat
See how to connect your Android app to the popular `Firebase`
* How to `Register` an account
* How to `Sign In` on account
* How to save data on `Database`

## Demo
![fbchat_demo](.screenshots/fbchat_demo.gif)
