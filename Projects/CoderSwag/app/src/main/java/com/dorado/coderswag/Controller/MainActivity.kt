package com.dorado.coderswag.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.dorado.coderswag.Adapters.CategoryRecyclerAdapter
import com.dorado.coderswag.R
import com.dorado.coderswag.Services.DataService
import com.dorado.coderswag.Utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

  lateinit var adapter: CategoryRecyclerAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    adapter = CategoryRecyclerAdapter(this, DataService.categories) { category ->
      val productIntent = Intent(this, ProductsActivity::class.java)
      productIntent.putExtra(EXTRA_CATEGORY, category.title)
      startActivity(productIntent)
    }
    listViewCategory.adapter = adapter

    val layoutManager = LinearLayoutManager(this)
    listViewCategory.layoutManager = layoutManager
    listViewCategory.setHasFixedSize(true)


  }
}