package com.dorado.coderswag.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.dorado.coderswag.Model.Category
import com.dorado.coderswag.R

class CategoryAdapter(val context: Context, val categories: List<Category>) : BaseAdapter() {

  // Base Adapter Functions

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    val categoryView: View
    val holder: ViewHolder

    // This will recycle the views
    if (convertView == null) {
      categoryView = LayoutInflater.from(context).inflate(R.layout.category_list_item, null)
      holder = ViewHolder()
      holder.imgCategory = categoryView.findViewById(R.id.imgCategory)
      holder.txtName = categoryView.findViewById(R.id.txtName)
      categoryView.tag = holder
      println("i exist for the first time!")
    } else {
      holder = convertView.tag as ViewHolder
      categoryView = convertView
      println("Go green, recycle!")
    }

    val category = categories[position]
    val resourceId = context.resources.getIdentifier(category.image, "drawable", context.packageName)
    holder.txtName?.text = category.title
    holder.imgCategory?.setImageResource(resourceId)

    return categoryView
  }

  override fun getItem(position: Int): Any {
    return categories[position]
  }

  override fun getItemId(position: Int): Long {
    return 0
  }

  override fun getCount(): Int {
    return categories.count()
  }

  private class ViewHolder {

    var imgCategory: ImageView? = null
    var txtName: TextView? = null
  }
}