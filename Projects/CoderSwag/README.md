# Coder Swag
On this project you will be able to see how the `adapters` are used to populate a `list`, also how to create `custom` list views, and how can they be handled using our `custom Recycler View` and also the `Recycler View` that we declare as implementation on the `Gradle` file, also you will be able to see how a `lambda` is used to handle the taps on a layout item.

## Demo
![coder_swag_demo](.screenshots/coder_swag_demo.gif)
