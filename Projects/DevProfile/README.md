# DevProfile
This is a dummy app... just to let you see how are used the `Layouts` to create an app that looks great on different sizes of Android devices.

## Demo
### Phone
![devprofile_device_portrait](.screenshots/devprofile_device_portrait.png)

### Tablet (Portrait)
![devprofile_tablet_portrait](.screenshots/devprofile_tablet_portrait.png)

### Tablet (Landscape)
![devprofile_tablet_ladscape](.screenshots/devprofile_tablet_ladscape.png)
